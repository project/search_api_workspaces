<?php

namespace Drupal\search_api_workspaces\Plugin\search_api\datasource;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Plugin\search_api\datasource\ContentEntity;
use Drupal\search_api\Utility\Utility;
use Drupal\workspaces\WorkspaceInterface;

/**
 * Represents a datasource which exposes the content entities.
 *
 * @SearchApiDatasource(
 *   id = "workspaces_entity",
 *   deriver = "Drupal\search_api_workspaces\Plugin\search_api\datasource\WorkspacesContentEntityDeriver"
 * )
 */
class WorkspacesContentEntity extends ContentEntity {

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $ids) {
    $allowed_languages = $this->getLanguages();
    // Always allow items with undefined language. (Can be the case when
    // entities are created programmatically.)
    $allowed_languages[LanguageInterface::LANGCODE_NOT_SPECIFIED] = TRUE;
    $allowed_languages[LanguageInterface::LANGCODE_NOT_APPLICABLE] = TRUE;

    // Something like ['145:en:12'].
    foreach ($ids as $item_id) {
      $keys = explode(':', $item_id);
      // This can only happen if someone passes an invalid ID, since we always
      // include a language code. Still, no harm in guarding against bad input.
      if ($keys === FALSE || empty($keys)) {
        continue;
      }
      // Expected format: ENTITY_ID:LANGCODE:REVISION_ID .
      $entity_id = $keys[0];
      $langcode = $keys[1];
      $revision_id = $keys[2];
      if (isset($allowed_languages[$langcode])) {
        $entity_ids[$revision_id][$item_id] = $langcode;
      }
    }

    $items = [];

    /** @var \Drupal\Core\Entity\ContentEntityInterface[] $entities */
    $entities = $this->getEntityStorage()->loadMultipleRevisions(array_keys($entity_ids));

    $allowed_bundles = $this->getBundles();
    foreach ($entity_ids as $entity_id => $langcodes) {
      if (empty($entities[$entity_id]) || !isset($allowed_bundles[$entities[$entity_id]->bundle()])) {
        continue;
      }
      foreach ($langcodes as $item_id => $langcode) {
        if ($entities[$entity_id]->hasTranslation($langcode)) {
          $items[$item_id] = $entities[$entity_id]->getTranslation($langcode)->getTypedData();
        }
      }
    }

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemId(ComplexDataInterface $item) {
    if ($entity = $this->getEntity($item)) {
      $enabled_bundles = $this->getBundles();
      if (isset($enabled_bundles[$entity->bundle()])) {
        return $entity->id() . ':' . $entity->language()->getId();
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getPartialItemIds($page = NULL, array $bundles = NULL, array $languages = NULL) {
    // These would be pretty pointless calls, but for the sake of completeness
    // we should check for them and return early. (Otherwise makes the rest of
    // the code more complicated.)
    if (($bundles === [] && !$languages) || ($languages === [] && !$bundles)) {
      return NULL;
    }
    /** @var \Drupal\workspaces\WorkspaceStorage $workspace_storage */
    $workspace_storage = \Drupal::service('entity_type.manager')->getStorage('workspace');
    $workspace_repository = \Drupal::service('workspaces.repository');
    $workspaces = $workspace_repository->loadTree();
    $workspaces['live'] = 'live';
    /** @var \Drupal\workspaces\WorkspaceManagerInterface $workspace_manager */
    $workspace_manager = \Drupal::service('workspaces.manager');
    $initial_workspace = $workspace_manager->getActiveWorkspace();
    $item_ids = [];
    foreach ($workspaces as $workspace_id => $tree_info) {
      if ($workspace_id === 'live') {
        $workspace_manager->switchToLive();
      }
      else {
        $workspace = $workspace_storage->load($workspace_id);
        $workspace_manager->setActiveWorkspace($workspace);
      }
      $item_ids += $this->getPartialItemIdsInWorkspace($workspace_id, $page, $bundles, $languages);
    }

    if ($initial_workspace) {
      $workspace_manager->setActiveWorkspace($initial_workspace);
    }
    else {
      $workspace_manager->switchToLive();
    }

    return empty($item_ids) ? NULL : $item_ids;
  }

  /**
   * Get item ids per active workspace.
   */
  protected function getPartialItemIdsInWorkspace($workspace_id, $page = NULL, array $bundles = NULL, array $languages = NULL) {
    $item_ids = [];

    $select = $this->getEntityTypeManager()
      ->getStorage($this->getEntityTypeId())
      ->getQuery();

    // When tracking items, we never want access checks.
    $select->accessCheck(FALSE);

    // Build up the context for tracking the last ID for this batch page.
    $batch_page_context = [
      'index_id' => $this->getIndex()->id(),
      // The derivative plugin ID includes the entity type ID.
      'datasource_id' => $this->getPluginId(),
      'bundles' => $bundles,
      'languages' => $languages,
    ];
    $context_key = Crypt::hashBase64(serialize($batch_page_context));
    $last_ids = $this->getState()->get(self::TRACKING_PAGE_STATE_KEY, []);

    // We want to determine all entities of either one of the given bundles OR
    // one of the given languages. That means we can't just filter for
    // $bundles if $languages is given. Instead, we have to filter for all
    // bundles we might want to include and later sort out those for which
    // we want only the translations in $languages and those (matching
    // $bundles) where we want all (enabled) translations.
    if ($this->hasBundles()) {
      $bundle_property = $this->getEntityType()->getKey('bundle');
      if ($bundles && !$languages) {
        $select->condition($bundle_property, $bundles, 'IN');
      }
      else {
        $enabled_bundles = array_keys($this->getBundles());
        // Since this is also called for removed bundles/languages,
        // $enabled_bundles might not include $bundles.
        if ($bundles) {
          $enabled_bundles = array_unique(array_merge($bundles, $enabled_bundles));
        }
        if (count($enabled_bundles) < count($this->getEntityBundles())) {
          $select->condition($bundle_property, $enabled_bundles, 'IN');
        }
      }
    }

    if (isset($page)) {
      $page_size = $this->getConfigValue('tracking_page_size');
      assert($page_size, 'Tracking page size is not set.');
      $entity_id = $this->getEntityType()->getKey('id');

      // If known, use a condition on the last tracked ID for paging instead
      // of the offset, for performance reasons on large sites.
      $offset = $page * $page_size;
      if ($page > 0) {
        // We only handle the case of picking up from where the last page left
        // off. (This will cause an infinite loop if anyone ever wants to
        // index Search API tasks in an index, so check for that to be on the
        // safe side.)
        if (isset($last_ids[$context_key])
          && $last_ids[$context_key]['page'] == ($page - 1)
          && $this->getEntityTypeId() !== 'search_api_task') {
          $select->condition($entity_id, $last_ids[$context_key]['last_id'], '>');
          $offset = 0;
        }
      }
      $select->range($offset, $page_size);

      // For paging to reliably work, a sort should be present.
      $select->sort($entity_id);
    }

    $entity_ids = $select->execute();
    if (!$entity_ids) {
      if (isset($page)) {
        // Clean up state tracking of last ID.
        unset($last_ids[$context_key]);
        $this->getState()->set(self::TRACKING_PAGE_STATE_KEY, $last_ids);
      }
      return [];
    }

    // Remember the last tracked ID for the next call.
    if (isset($page)) {
      $last_ids[$context_key] = [
        'page' => (int) $page,
        'last_id' => end($entity_ids),
      ];
      $this->getState()->set(self::TRACKING_PAGE_STATE_KEY, $last_ids);
    }

    // For all loaded entities, compute all their item IDs (one for each
    // translation we want to include). For those matching the given bundles
    // (if any), we want to include translations for all enabled languages.
    // For all other entities, we just want to include the translations for
    // the languages passed to the method (if any).
    $enabled_languages = array_keys($this->getLanguages());
    // As above for bundles, $enabled_languages might not include $languages.
    if ($languages) {
      $enabled_languages = array_unique(array_merge($languages, $enabled_languages));
    }
    // Also, we want to always include entities with unknown language.
    $enabled_languages[] = LanguageInterface::LANGCODE_NOT_SPECIFIED;
    $enabled_languages[] = LanguageInterface::LANGCODE_NOT_APPLICABLE;

    foreach ($this->getEntityStorage()->loadMultiple($entity_ids) as $entity_id => $entity) {
      $translations = array_keys($entity->getTranslationLanguages());
      $translations = array_intersect($translations, $enabled_languages);
      // If only languages were specified, keep only those translations
      // matching them.
      // If bundles were also specified, keep all (enabled) translations
      // for those entities that match those bundles.
      if ($languages !== NULL
        && (!$bundles || !in_array($entity->bundle(), $bundles))) {
        $translations = array_intersect($translations, $languages);
      }
      if ($entity->getEntityType()->isRevisionable()) {
        $revision_id = $entity->getRevisionId();
        foreach ($translations as $langcode) {
          if (!isset($item_ids[$revision_id . ':' . $langcode])) {
            $item_ids[$revision_id . ':' . $langcode] = "$entity_id:$langcode:$revision_id";
          }
        }
      }
    }

    if (Utility::isRunningInCli()) {
      // When running in the CLI, this might be executed for all entities from
      // within a single process. To avoid running out of memory, reset the
      // static cache after each batch.
      $this->getEntityMemoryCache()->deleteAll();
    }

    return $item_ids;
  }

  /**
   * {@inheritdoc}
   */
  public static function getIndexesForEntity(ContentEntityInterface $entity) {
    $datasource_id = 'workspaces_entity:' . $entity->getEntityTypeId();
    $entity_bundle = $entity->bundle();
    $has_bundles = $entity->getEntityType()->hasKey('bundle');

    // Needed for PhpStorm. See https://youtrack.jetbrains.com/issue/WI-23395.
    /** @var \Drupal\search_api\IndexInterface[] $indexes */
    $indexes = Index::loadMultiple();

    foreach ($indexes as $index_id => $index) {
      // Filter out indexes that don't contain the datasource in question.
      if (!$index->isValidDatasource($datasource_id)) {
        unset($indexes[$index_id]);
      }
      elseif ($has_bundles) {
        // If the entity type supports bundles, we also have to filter out
        // indexes that exclude the entity's bundle.
        $config = $index->getDatasource($datasource_id)->getConfiguration();
        if (!Utility::matches($entity_bundle, $config['bundles'])) {
          unset($indexes[$index_id]);
        }
      }
    }

    return $indexes;
  }

}
