<?php

namespace Drupal\search_api_workspaces\Plugin\search_api\processor;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;
use Drupal\search_api\SearchApiException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds the workspaces the content is associated with.
 *
 * @SearchApiProcessor(
 *   id = "workspace_association",
 *   label = @Translation("Workspace association"),
 *   description = @Translation("Adds the workspaces the item is related to."),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class WorkspaceAssociation extends ProcessorPluginBase {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $processor->setEntityTypeManager($container->get('entity_type.manager'));

    return $processor;
  }

  /**
   * Retrieves the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity repository.
   */
  public function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * Sets the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The new entity repository.
   *
   * @return $this
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Workspace association'),
        'description' => $this->t('The workspaces the item is associated with.'),
        'type' => 'string',
        'settings' => [
          'target_type' => 'workspace',
        ],
        'processor_id' => $this->getPluginId(),
        'is_list' => TRUE,
      ];
      $properties['workspace_association'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    try {
      $entity = $item->getOriginalObject()->getValue();
    }
    catch (SearchApiException $e) {
      return;
    }
    if (!($entity instanceof ContentEntityInterface)) {
      return;
    }

    $query = \Drupal::database()->select('workspace_association', 'wa');
    $query->fields('wa', ['workspace'])
      ->condition('target_entity_type_id', $entity->getEntityTypeId())
      ->condition('target_entity_id', $entity->id())
      ->condition('target_entity_revision_id', $entity->getRevisionId());

    // Get all the workspaces this revision is associated with.
    $associated_workspaces = $query->execute()->fetchAll();
    if (empty($associated_workspaces)) {
      return;
    }

    $fields = $item->getFields();
    $fields = $this->getFieldsHelper()
      ->filterForPropertyPath($fields, NULL, 'workspace_association');
    foreach ($fields as $field) {
      // Add related workspaces as indexed value.
      foreach ($associated_workspaces as $row) {
        $field->addValue($row->workspace);
      }
    }
  }

}
