<?php

namespace Drupal\search_api_workspaces\Plugin\views\filter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\search_api\Plugin\views\filter\SearchApiFilterTrait;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\workspaces\WorkspaceManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\workspaces\WorkspaceRepository;

/**
 * Defines a filter for filtering on the associated workspaces of items.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("search_api_workspace")
 */
class SearchApiWorkspace extends InOperator {

  use SearchApiFilterTrait;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The workspaces manager.
   *
   * @var \Drupal\workspaces\WorkspaceManagerInterface
   */
  protected $workspaceManager;

  /**
   * The workspaces repository.
   *
   * @var \Drupal\workspaces\WorkspaceRepository
   */
  protected $workspaceRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $processor->setEntityTypeManager($container->get('entity_type.manager'));
    $processor->setWorkspaceManager($container->get('workspaces.manager'));
    $processor->setWorkspaceRepository($container->get('workspaces.repository'));

    return $processor;
  }

  /**
   * Retrieves the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity repository.
   */
  public function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * Sets the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The new entity repository.
   *
   * @return $this
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    return $this;
  }

  /**
   * Retrieves the workspace manager.
   *
   * @return \Drupal\workspaces\WorkspaceManagerInterface
   *   The workspace manager.
   */
  public function getWorkspaceManager() {
    return $this->workspaceManager;
  }

  /**
   * Sets the workspace manager.
   *
   * @param \Drupal\workspaces\WorkspaceManagerInterface $workspaceManager
   *   The new workspace manager.
   */
  public function setWorkspaceManager(WorkspaceManagerInterface $workspaceManager) {
    $this->workspaceManager = $workspaceManager;
  }

  /**
   * Retrieves the workspace repository.
   *
   * @return \Drupal\workspaces\WorkspaceRepository
   *   The workspace repository.
   */
  public function getWorkspaceRepository() {
    return $this->workspaceRepository;
  }

  /**
   * Sets the workspace repository.
   *
   * @param \Drupal\workspaces\WorkspaceRepository $workspaceRepository
   *   The new workspace repository.
   */
  public function setWorkspaceRepository(WorkspaceRepository $workspaceRepository) {
    $this->workspaceRepository = $workspaceRepository;
  }

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {

    $workspaces = $this->workspaceRepository->loadTree();
    // Initialise with placeholder select options.
    $workspace_select = [
      '***WORKSPACE_active_workspace***' => $this->t('Active workspace'),
    ];
    array_walk(
      $workspaces,
      function ($workspace, $workspace_id) use (&$workspace_select) {
        $workspace_select[$workspace_id] = $workspace_id;
      }
    );

    $this->valueOptions = $workspace_select;

    return $this->valueOptions;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $substitutions = $this->queryWorkspaceSubstitutions();

    foreach ($this->value as $i => $value) {
      if (isset($substitutions[$value])) {
        $this->value[$i] = $substitutions[$value];
      }
    }

    parent::query();
  }

  /**
   * Return a mapping of substitution strings to real values.
   *
   * @return array
   *   The substitution mapping.
   */
  protected function queryWorkspaceSubstitutions() {
    return [
      '***WORKSPACE_active_workspace***' => $this->workspaceManager->hasActiveWorkspace() ? $this->workspaceManager->getActiveWorkspace()->id() : FALSE,
    ];
  }

}
