<?php

namespace Drupal\search_api_workspaces\EventSubscriber;

use Drupal\search_api\Event\MappingViewsHandlersEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\search_api\Event\SearchApiEvents;

/**
 * Event subscriptions for events dispatched by Search API.
 */
class SearchApiWorkspacesSubscriber implements EventSubscriberInterface {

  /**
   * Returns an array of event names this subscriber listens to.
   *
   * @return array
   *   The event names to listen to
   */
  public static function getSubscribedEvents() {
    return [
      SearchApiEvents::MAPPING_VIEWS_HANDLERS => 'modifyViewsHandlers',
    ];
  }

  /**
   * Alter the views handler mapping.
   *
   * @param \Drupal\search_api\Event\MappingViewsHandlersEvent $event
   *   The event object contains the mapping.
   */
  public function modifyViewsHandlers(MappingViewsHandlersEvent $event) {
    // Returns a reference to the mapping.
    $mapping = &$event->getHandlerMapping();

    // Alter mapping, which switches out filter to custom workspace view filter,
    // in Drupal\search_api_workspaces\Plugin\views\filter\SearchApiWorkspace.
    $mapping['entity:workspace'] = [
      'argument' => [
        'id' => 'search_api',
      ],
      'filter' => [
        'id' => 'search_api_workspace',
        'allow empty' => FALSE,
      ],
      'sort' => [
        'id' => 'search_api',
      ],
    ];
  }

}
