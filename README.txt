CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
This module provides a Search API datasource plugin, fields and filter to
index workspace specific content entities.
It creates items with the ID pattern of {entity_id}:{langcode}:{revision_id}.
The functionality included focuses on inherited content from parent workspaces,
providing a workspace association field and filter to allow filtering based
on the currently selected workspace, and return all associated workspace
revisions.

REQUIREMENTS
------------
This module requires the following modules:

 * Search API (https://www.drupal.org/project/search_api)
 * Workspaces (core)

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. For further
information, see:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-modules

CONFIGURATION
-------------
After installation, create a search index using one of the datasources
provided by this module, which are all prepended with "Workspace revisions:".

Make sure you add the following global fields to any workspace revisions index:
- Source Workspace
- Workspace association

When adding/updating views, you can add fields or filter by the above two
fields. In "Advanced > Caching", it is recommended to choose
"Search API (tag-based)".

MAINTAINERS
-----------
Current maintainers:
  * Nick O'Sullivan (nbaosullivan) - https://www.drupal.org/u/nbaosullivan
  * Fran Garcia (fjgarlin) - https://www.drupal.org/u/fjgarlin
